node-ac-telemetry
=================

Node.js library for accessing [Assetto Corsa](http://www.assettocorsa.net/) 
live telemetry data using the [shared memory api](http://www.assettocorsa.net/forum/index.php?threads/shared-memory-reference.3352/).

# Installation

```sh
# once published to npm
# npm install --save ac-telemetry

# for now
npm install --save git@bitbucket.org:simetry/node-ac-telemetry.git#0.1.0
```

# Usage

```js
var ac = require('ac-telemetry');

console.log('player name', ac.static.playerName);
setInterval(function() {
  console.log(
    'current time', ac.graphics.iCurrentTime,
    'speed', ac.physics.speedKmh
  );
}, 32);
```

# API

The API is based on [shared memory structs](src/memory_layout.h) exposed by Assetto Corsa:

`static` - `SPageFileStatic`  
`physics` - `SPageFilePhysics`  
`graphics` - `SPageFileGraphic`  

The only changes made are in `graphics.status` and `graphics.session` which are strings.

|             Property             |  Type  | Unit |                                       Description                                        |
| -------------------------------- | :----: | :--: | ---------------------------------------------------------------------------------------- |
| __static__                       |        |      |                                                                                          |
| `static.acVersion`               | String |      | Assetto Corsa version                                                                    |
| `static.carModel`                | String |      | name of the car                                                                          |
| `static.maxFuel`                 | Number |  l   | fuel tank size in                                                                        |
| `static.maxPower`                | Number |  ?   | max engine power                                                                         |
| `static.maxRpm`                  | Number |      | max RPM                                                                                  |
| `static.maxTorque`               | Number |  ?   | max Torque                                                                               |
| `static.numberOfSessions`        | Number |      | number of sessions in a race weekend                                                     |
| `static.numCars`                 | Number |      | number of cars loaded                                                                    |
| `static.playerName`              | String |      | player first name                                                                        |
| `static.playerNick`              | String |      | player nickname                                                                          |
| `static.playerSurname`           | String |      | player last name                                                                         |
| `static.sectorCount`             | Number |      | number of sectors on track                                                               |
| `static.smVersion`               | String |      | shared memory interface version                                                          |
| `static.suspensionMaxTravel`     | Array  |  m   | max suspension travel                                                                    |
| `static.track`                   | String |      | track name                                                                               |
| `static.tyreRadius`              | Array  |  m   | tyre radius                                                                              |
| __physics__                      |        |      |                                                                                          |
| `physics.abs`                    | Number |      | abs strength [0-1]                                                                       |
| `physics.accG`                   | Array  |  G   | acceleration relative 3d vector                                                          |
| `physics.brake`                  | Number |      | brake pedal position [0-1]                                                               |
| `physics.camberRAD`              | Array  | rad  | camber of each wheel                                                                     |
| `physics.carDamage`              | Array  |      | amount of damage on various elements                                                     |
| `physics.cgHeight`               | Number |  m   | center of geavity height                                                                 |
| `physics.drs`                    | Number |      | whether drs is active [0-1?]                                                             |
| `physics.fuel`                   | Number |  l   | fuel left                                                                                |
| `physics.gas`                    | Number |      | gas pedal position [0-1]                                                                 |
| `physics.gear`                   | Number |      | selected gear, 0: reverse, 1: neutral, 2: 1st, ...                                       |
| `physics.heading`                | Number | rad  | heading                                                                                  |
| `physics.numberOfTyresOut`       | Number |      | how many tyres are off the track surface                                                 |
| `physics.packetId`               | Number |      | telemetry packet id                                                                      |
| `physics.pitch`                  | Number | rad  | pitch]                                                                                   |
| `physics.pitLimiterOn`           | Number |      | whether the pit limiter is on [0-1]                                                      |
| `physics.roll`                   | Number | rad  | roll                                                                                     |
| `physics.rpms`                   | Number |      | RPM                                                                                      |
| `physics.speedKmh`               | Number | km/h | speed                                                                                    |
| `physics.steerAngle`             | Number |      | amount of steering relative to max steering lock [-1.0 - 1.0]                            |
| `physics.suspensionTravel`       | Array  |  m   | current suspension travel                                                                |
| `physics.tc`                     | Number |      | traction control strength [0-1]                                                          |
| `physics.tyreCoreTemperature`    | Array  |  C   | tyre temperatures in the center                                                          |
| `physics.tyreDirtyLevel`         | Array  |  ?   | dirt level of each tyre                                                                  |
| `physics.tyreWear`               | Array  |      | percentage of tyre left on each wheel                                                    |
| `physics.velocity`               | Array  | m/s  | velocity absolute 3d vector                                                              |
| `physics.wheelAngularSpeed`      | Array  | m/s  | wheel angular speed                                                                      |
| `physics.wheelLoad`              | Array  |  N   | load on each wheel                                                                       |
| `physics.wheelSlip`              | Array  |  ?   | slip amounts                                                                             |
| `physics.wheelsPressure`         | Array  | psi  | tyre pressures                                                                           |
| __graphics__                     |        |      |                                                                                          |
| `graphics.bestTime`              | String |      | session best lap time                                                                    |
| `graphics.carCoordinates`        | Array  |  m   | car position absolute 3d vector                                                          |
| `graphics.completedLaps`         | Number |      | number of completed laps                                                                 |
| `graphics.currentSectorIndex`    | Number |      | current sector                                                                           |
| `graphics.currentTime`           | String |      | current lap time                                                                         |
| `graphics.distanceTraveled`      | Number |  m   | total distance traveled                                                                  |
| `graphics.iBestTime`             | Number |  ms  | session best lap time                                                                    |
| `graphics.iCurrentTime`          | Number |  ms  | current lap time                                                                         |
| `graphics.iLastTime`             | Number |  ms  | last lap time                                                                            |
| `graphics.isInPit`               | Number |      | whether the player is in the garage [0-1]                                                |
| `graphics.lastSectorTime`        | Number |  ms  | previous sector time                                                                     |
| `graphics.lastTime`              | String |      | last lap time                                                                            |
| `graphics.normalizedCarPosition` | Number |      | car position relative to the track length [0-1]                                          |
| `graphics.numberOfLaps`          | Number |      | session distance (eg. race laps)                                                         |
| `graphics.packetId`              | Number |      | telemetry packet id                                                                      |
| `graphics.position`              | Number |      | position at the end of last lap                                                          |
| `graphics.replayTimeMultiplier`  | Number |      | replay time multiplier                                                                   |
| `graphics.session`               | String |      | session type, one of: unknown, practice, qualify, race, hotlap, time_attack, drift, drag |
| `graphics.sessionTimeLeft`       | Number |  ms  | time left in the session                                                                 |
| `graphics.split`                 | String |      | previous sector time                                                                     |
| `graphics.status`                | String |      | game state, one of: unknown, off, replay, live, pause                                    |
| `graphics.tyreCompound`          | String |      | tyre compound name                                                                       |

## Notes

Relative 3d vector orientation:

- x: -left, +right
- y: -low, +high
- z: -back. +front

Wheel order:

- front left
- front right
- rear left
- rear right

## Questions to Kunos

- What are the `static.maxPower`, `static.maxTorque` units?
- What's in the `physics.carDamage` array?
- What do `physics.tyreDirtyLevel` values represent?
- Why is `physics.wheelAngularSpeed` always 0
