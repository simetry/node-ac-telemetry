{
  "targets": [
    {
      "target_name": "addon",
      "sources": [
        "src/to_utf8.cc",
        "src/addon.cc"
      ],
      'cflags_cc': ['-std=c++11'],
      'cflags_cc!': ['-fno-rtti'],
      'msvs_settings': {
        'VCCLCompilerTool': {
          'AdditionalOptions': ['/EHsc']
        }
      },
      "include_dirs": [
        "<!(node -e \"require('nan')\")"
      ]
    }
  ]
}
