#include "to_utf8.h"

#include <Windows.h>

using namespace std;

string to_utf8(const wchar_t* buffer, long length=-1) {
  if (length < 0) {
    length = wcslen(buffer);
  }

  int outSize = ::WideCharToMultiByte(CP_UTF8, 0, buffer, length, NULL, 0, NULL, NULL);
  if (outSize == 0) return "";

  string outString;
  outString.resize(outSize);

  char* outBuffer = const_cast<char*>(outString.c_str());
  ::WideCharToMultiByte(CP_UTF8,0, buffer,length, outBuffer, outSize, NULL, NULL);

  return outString;
}

string to_utf8(const wstring& str) {
  return to_utf8(str.c_str(), (int)str.size());
}
