#include <nan.h>

#include "to_utf8.h"
#include "shared_memory.h"
#include "memory_layout.h"

using namespace v8;


SharedMemory<SPageFilePhysics> physicsSM("Local\\acpmf_physics");
SharedMemory<SPageFileGraphic> graphicsSM("Local\\acpmf_graphics");
SharedMemory<SPageFileStatic>  staticSM("Local\\acpmf_static");

NAN_GETTER(GetStatic) {
  NanScope();

  Local<Object> obj = NanNew<Object>();

  obj->Set(NanNew("numberOfSessions"), NanNew(staticSM->numberOfSessions));
  obj->Set(NanNew("numCars"), NanNew(staticSM->numCars));
  obj->Set(NanNew("sectorCount"), NanNew(staticSM->sectorCount));
  obj->Set(NanNew("maxTorque"), NanNew(staticSM->maxTorque));
  obj->Set(NanNew("maxPower"), NanNew(staticSM->maxPower));
  obj->Set(NanNew("maxRpm"), NanNew(staticSM->maxRpm));
  obj->Set(NanNew("maxFuel"), NanNew(staticSM->maxFuel));

  obj->Set(NanNew("smVersion"), NanNew(to_utf8(staticSM->smVersion)));
  obj->Set(NanNew("acVersion"), NanNew(to_utf8(staticSM->acVersion)));
  obj->Set(NanNew("carModel"), NanNew(to_utf8(staticSM->carModel)));
  obj->Set(NanNew("track"), NanNew(to_utf8(staticSM->track)));
  obj->Set(NanNew("playerName"), NanNew(to_utf8(staticSM->playerName)));
  obj->Set(NanNew("playerSurname"), NanNew(to_utf8(staticSM->playerSurname)));
  obj->Set(NanNew("playerNick"), NanNew(to_utf8(staticSM->playerNick)));

  auto suspensionMaxTravel = NanNew<Array>(4);
  auto tyreRadius = NanNew<Array>(4);

  obj->Set(NanNew("suspensionMaxTravel"), suspensionMaxTravel);
  obj->Set(NanNew("tyreRadius"), tyreRadius);

  for (int i = 0; i < 4; ++i) {
    suspensionMaxTravel->Set(i, NanNew(staticSM->suspensionMaxTravel[i]));
    tyreRadius->Set(i, NanNew(staticSM->tyreRadius[i]));
  }

  NanReturnValue(obj);
}

const char* AC_STATUS_NAMES[] = {
  "off",
  "replay",
  "live",
  "pause"
};

const char* AC_SESSION_TYPE_NAMES[] = {
  "practice",
  "qualify",
  "race",
  "hotlap",
  "time_attack",
  "drift",
  "drag"
};

NAN_GETTER(GetGraphics) {
  NanScope();

  Local<Object> obj = NanNew<Object>();

  const char* statusName = "unknown";
  const char* sessionName = "unknown";

  if (graphicsSM->status >= AC_OFF && graphicsSM->status <= AC_PAUSE) {
    statusName = AC_STATUS_NAMES[graphicsSM->status];
  }

  if (graphicsSM->session >= AC_PRACTICE && graphicsSM->session <= AC_DRAG) {
    sessionName = AC_SESSION_TYPE_NAMES[graphicsSM->session];
  }

  obj->Set(NanNew("status"), NanNew(statusName));
  obj->Set(NanNew("session"), NanNew(sessionName));

  obj->Set(NanNew("packetId"), NanNew(graphicsSM->packetId));
  obj->Set(NanNew("completedLaps"), NanNew(graphicsSM->completedLaps));
  obj->Set(NanNew("position"), NanNew(graphicsSM->position));
  obj->Set(NanNew("iCurrentTime"), NanNew(graphicsSM->iCurrentTime));
  obj->Set(NanNew("iLastTime"), NanNew(graphicsSM->iLastTime));
  obj->Set(NanNew("iBestTime"), NanNew(graphicsSM->iBestTime));
  obj->Set(NanNew("sessionTimeLeft"), NanNew(graphicsSM->sessionTimeLeft));
  obj->Set(NanNew("distanceTraveled"), NanNew(graphicsSM->distanceTraveled));
  obj->Set(NanNew("isInPit"), NanNew(graphicsSM->isInPit));
  obj->Set(NanNew("currentSectorIndex"),
           NanNew(graphicsSM->currentSectorIndex));
  obj->Set(NanNew("lastSectorTime"), NanNew(graphicsSM->lastSectorTime));
  obj->Set(NanNew("numberOfLaps"), NanNew(graphicsSM->numberOfLaps));
  obj->Set(NanNew("replayTimeMultiplier"),
           NanNew(graphicsSM->replayTimeMultiplier));
  obj->Set(NanNew("normalizedCarPosition"),
           NanNew(graphicsSM->normalizedCarPosition));

  obj->Set(NanNew("currentTime"), NanNew(to_utf8(graphicsSM->currentTime)));
  obj->Set(NanNew("lastTime"), NanNew(to_utf8(graphicsSM->lastTime)));
  obj->Set(NanNew("bestTime"), NanNew(to_utf8(graphicsSM->bestTime)));
  obj->Set(NanNew("split"), NanNew(to_utf8(graphicsSM->split)));
  obj->Set(NanNew("tyreCompound"), NanNew(to_utf8(graphicsSM->tyreCompound)));

  auto carCoordinates = NanNew<Array>(3);
  obj->Set(NanNew("carCoordinates"), carCoordinates);

  for (int i = 0; i < 3; ++i) {
    carCoordinates->Set(i, NanNew(graphicsSM->carCoordinates[i]));
  }

  NanReturnValue(obj);
}

NAN_GETTER(GetPhysics) {
  NanScope();

  Local<Object> obj = NanNew<Object>();

  obj->Set(NanNew("packetId"), NanNew(physicsSM->packetId));
  obj->Set(NanNew("gas"), NanNew(physicsSM->gas));
  obj->Set(NanNew("brake"), NanNew(physicsSM->brake));
  obj->Set(NanNew("fuel"), NanNew(physicsSM->fuel));
  obj->Set(NanNew("gear"), NanNew(physicsSM->gear));
  obj->Set(NanNew("rpms"), NanNew(physicsSM->rpms));
  obj->Set(NanNew("steerAngle"), NanNew(physicsSM->steerAngle));
  obj->Set(NanNew("speedKmh"), NanNew(physicsSM->speedKmh));
  obj->Set(NanNew("drs"), NanNew(physicsSM->drs));
  obj->Set(NanNew("tc"), NanNew(physicsSM->tc));
  obj->Set(NanNew("heading"), NanNew(physicsSM->heading));
  obj->Set(NanNew("pitch"), NanNew(physicsSM->pitch));
  obj->Set(NanNew("roll"), NanNew(physicsSM->roll));
  obj->Set(NanNew("cgHeight"), NanNew(physicsSM->cgHeight));
  obj->Set(NanNew("numberOfTyresOut"), NanNew(physicsSM->numberOfTyresOut));
  obj->Set(NanNew("pitLimiterOn"), NanNew(physicsSM->pitLimiterOn));
  obj->Set(NanNew("abs"), NanNew(physicsSM->abs));

  auto velocity = NanNew<Array>(3);
  auto accG = NanNew<Array>(3);
  auto wheelSlip = NanNew<Array>(4);
  auto wheelLoad = NanNew<Array>(4);
  auto wheelsPressure = NanNew<Array>(4);
  auto wheelAngularSpeed = NanNew<Array>(4);
  auto tyreWear = NanNew<Array>(4);
  auto tyreDirtyLevel = NanNew<Array>(4);
  auto tyreCoreTemperature = NanNew<Array>(4);
  auto camberRAD = NanNew<Array>(4);
  auto suspensionTravel = NanNew<Array>(4);
  auto carDamage = NanNew<Array>(5);

  obj->Set(NanNew("velocity"), velocity);
  obj->Set(NanNew("accG"), accG);
  obj->Set(NanNew("wheelSlip"), wheelSlip);
  obj->Set(NanNew("wheelLoad"), wheelLoad);
  obj->Set(NanNew("wheelsPressure"), wheelsPressure);
  obj->Set(NanNew("wheelAngularSpeed"), wheelAngularSpeed);
  obj->Set(NanNew("tyreWear"), tyreWear);
  obj->Set(NanNew("tyreDirtyLevel"), tyreDirtyLevel);
  obj->Set(NanNew("tyreCoreTemperature"), tyreCoreTemperature);
  obj->Set(NanNew("camberRAD"), camberRAD);
  obj->Set(NanNew("suspensionTravel"), suspensionTravel);
  obj->Set(NanNew("carDamage"), carDamage);

  for (int i=0; i<5; ++i) {
    if (i < 3) {
      velocity->Set(i, NanNew(physicsSM->velocity[i]));
      accG->Set(i, NanNew(physicsSM->accG[i]));
    }
    if (i < 4) {
      wheelSlip->Set(i, NanNew(physicsSM->wheelSlip[i]));
      wheelLoad->Set(i, NanNew(physicsSM->wheelLoad[i]));
      wheelsPressure->Set(i, NanNew(physicsSM->wheelsPressure[i]));
      wheelAngularSpeed->Set(i, NanNew(physicsSM->wheelAngularSpeed[i]));
      tyreWear->Set(i, NanNew(physicsSM->tyreWear[i]));
      tyreDirtyLevel->Set(i, NanNew(physicsSM->tyreDirtyLevel[i]));
      tyreCoreTemperature->Set(i, NanNew(physicsSM->tyreCoreTemperature[i]));
      camberRAD->Set(i, NanNew(physicsSM->camberRAD[i]));
      suspensionTravel->Set(i, NanNew(physicsSM->suspensionTravel[i]));
      carDamage->Set(i, NanNew(physicsSM->carDamage[i]));
    }
    carDamage->Set(i, NanNew(physicsSM->carDamage[i]));
  }

  NanReturnValue(obj);
}


void Init(Handle<Object> exports, Handle<Object> module) {
  if (physicsSM.attach() && graphicsSM.attach() && staticSM.attach()) {
    Local<ObjectTemplate> tmpl = NanNew<ObjectTemplate>();
    tmpl->SetAccessor(NanNew("static"), GetStatic);
    tmpl->SetAccessor(NanNew("physics"), GetPhysics);
    tmpl->SetAccessor(NanNew("graphics"), GetGraphics);

    module->Set(NanNew("exports"), tmpl->NewInstance());
  }
  else {
    NanThrowError("failed opening shared memory");
  }
}

NODE_MODULE(addon, Init)
