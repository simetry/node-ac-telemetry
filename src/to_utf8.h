#ifndef TO_UTF8_H
#define TO_UTF8_H

#include <string>

std::string to_utf8(const wchar_t* buffer, long length);
std::string to_utf8(const std::wstring& str);

#endif TO_UTF8_H
