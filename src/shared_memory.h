#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H

#include <string>
#include <windows.h>
#include <tchar.h>


template<typename T>
class SharedMemory {
  T* fileData = NULL;
  HANDLE fileHandle = NULL;
  const std::string fileName;

  public:
    SharedMemory(const char* name) : fileName(name) {}
    SharedMemory(const std::string& name): fileName(name) {}

    ~SharedMemory() {
      if (fileData != NULL) UnmapViewOfFile(fileData);
      if (fileHandle != NULL) CloseHandle(fileHandle);
    }

    bool isAttached() { return fileData != NULL; }

    bool attach() {
      fileHandle = CreateFileMapping(
        INVALID_HANDLE_VALUE,
        NULL,
        PAGE_READWRITE,
        0,
        sizeof(T),
        fileName.c_str()
      );

      if (!fileHandle) return false;

      fileData = (T*)MapViewOfFile(
        fileHandle,
        FILE_MAP_READ,
        0,
        0,
        sizeof(T)
      );

      if (!fileData) {
        UnmapViewOfFile(fileHandle);
        fileHandle = NULL;
        return false;
      }

      return true;
    }

    T* operator->() const { return fileData; }
};

#endif // SHARED_MEMORY_H
